
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <LINK REL=StyleSheet HREF="../styles/fab-sys.css" 
	  TYPE="text/css" MEDIA=screen TITLE=fab>
    <title>
Fabbri Systems :: Home    </title>

  </head>

  <body>
	<table class="tblmain">
	<tr>
	  <td class="maintop-left">
Home   	  </td>
	  <td class="maintop">
	    <img src="fabbri-systems-300x80.gif" alt="Fabbri Systems">
   	  </td>
	</tr>

	<tr>
	  <td class="navcontain">
	  <center>
		<table class="nav"><tr><td>
		  <a href="index.php">Home</a>
<!--		  <a href="/systems/projects.php">Projects</a>-->
		  <a href="../pro/bb/index.html">Technical Forums</a>
		  <a href="contact.php">Contact</a>
		  <a href="../pro/index.php">About Aaron Fabbri</a>
		  </td></tr>
	   	</table>
	  <p class="counter">
		
4759	  </p>
	  <p class="gensmall">unique visitors since 01/30/2006</p>	 
	  </center>
 	  </td>

	<td class="content">

	<h2>Fabbri Systems</h3>
	<p class="margin1">
	<b>Fabbri Systems specializes in high performance, fault tolerant, and
scalable distributed systems.</b>  The services we provide include: 
</p>
	<h4>OS and Kernel Software Engineering</h4>
	<p class="margin3">
	We can help with your FreeBSD and Linux kernel development projects.
	We have specific expertise with device drivers, performance tuning,
	filesystems, and network stack/protocol implementation.
	</p>

	<h4>Enterprise IT Systems Architecture</h4> <p class="margin3">Need an
	expert to design your next-generation IT infrastructure?  We leverage
open-source and commercial products to design scalable, maintainable,
cost-effective IT solutions which pay for themselves. Whether you're starting
from scratch or moving forward with existing systems, we can help you do things
The Right Way.
</p>
	<h4>Systems Development Staff Training</h4>
	<p class="margin3">
	Starting a new IT or software development project based on an
unfamiliar technology can eat up a lot of time and money in the initial
research phase.  Even worse, flawed initial implementations can require rewrites
or cause problems in the field.  Hiring an expert to work with your staff for a
couple of hours a week can significantly improve the efficiency of your development
project.  We can give presentations which will get your team quickly up to speed, and
provide ongoing leadership to make sure you do things The Right Way.
</p>
	<h4>Crunch Time Consulting</h4>
	<p class="margin3">
	Fabbri Systems provides on-demand expertise which can be allocated 
	to avoid development schedule crises.  Contact us before your deadline
and we can help your project succeed.
	</p> 
	
	<h4>You Get What You Pay For</h4>
	<p class="margin3">
	We can base our fees on a checklist of deliverable items to ensure a return on your
investment.  We also have flat hourly rates available.
	</p>
	<h4>For More Information</h4>
	<p class="margin3"><a href="contact.php">Contact us</a>.</p>

	<p>&nbsp;</p>
 
   	</td></tr>
	</table>

	<p class="margin5">
	  <b>Questions?</b> <a href="contact.php">Contact</a> Us.
	</p>


<p> &nbsp; </p>
<p> &nbsp; </p>

<!-- google analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4159952-2");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<!-- END google analytics -->
  </body>
</html>

	
