


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<title>
Aaron and Jocelyn :: Lodging</title>
<LINK REL=StyleSheet HREF="wed-style.css" TYPE="text/css" MEDIA=screen TITLE=wed>
</head>

<body>
	<table width="800"  border="0" cellspacing="0" cellpadding="0">
		<tr><td width="800" colspan="2">
<img src="arrowhead-lk-sunset-hdr-800.jpg">			</td>
		</tr>
		<tr><td width="800" colspan="2">
				<img src="aj-shadow-800.png">
			</td>
		</tr>
		<tr><td width="800" bgcolor="#cccccc">

	<tr>
	<td class="navcontain">
	  <center>
		<table class="nav"><tr><td>
		  <a href="index.php">Home</a>
		  <a href="story.php">The Story</a>
		  <a href="../blog/exit.php?url_id=113&amp;entry_id=10">Engagement Party</a>
		  <a href="event.php">Main Event</a>
		  <a href="lodging.php">Lodging</a>
		  <a href="registry.php">Registry</a>
		  <a href="contact.php">Contact Us</a>
		  <a href="../main.php">Fabbrication</a>
		  </td></tr>
	   	</table>
	  <p class="counter">
		
2143	  </p>
	  <p class="gensmall">unique visitors since 10/12/2004</p>	 
	<p>&nbsp;</p>	
	<h4>Extra Links</h4>
	<p><a
href="http://www.fs.fed.us/r6/centraloregon/recreation/fishing/lake-reservoir/crescent.shtml">Crescent
Lake Info</a>
	<p><a href="http://tinyurl.com/cexow">Directions from Carson City, NV to
Crescent Lake, OR</a>
	  </center>
 	  </td>

	<td class="content">
<h1>Lodging</h1>	<p class="margin2">
	We recommend that our guests make a vacation out of our wedding.   The
	wedding is happening in the Willamette Pass area, in the Cascade
	Mountains east of Eugene, Oregon.  
	</p>
	<p class="margin2">
	There are multiple options for staying in the Willamette Pass area.  We
	have reserved multiple cabins at the rustic Crescent Lake Resort that we
	are staying at.  There are also about 4-5 other Inns in the vicinity.  	
	</p>


	<h3>Crescent Lake Resort</h3>

	<p>
	This is where most of us are staying.  It is a beautiful mountain lake
	setting, very outdoorsy.  We've reserved a bunch of cabins,
	which have a minimium three night stay (we're staying Friday, Saturday, and
	Sunday nights).
	</p>

	<h4>Reservation Terms</h4>
	<p>
	Advance deposit on the cabins we've reserved is due 3/10/2005.  Please
	<a href="contact.php">let us know</a> before that date if you'd like to
	take an open cabin below.
	Advance deposit is the cost of one night's stay.   Cancellation is
	allowed up to 30 days before your stay, with a $10 fee.
	</p>
	
	<h4>Reserved Cabins</h4>
	
	<p>The following cabins are available for Friday, Saturday, and Sunday
	nights.  There is a three night minimum on weekends, so each cabin needs
	to be taken all three days.
	</p>
	<table class="it">
	<tr class="header1">
	<td width="25">Cabin No.</td><td width="80">Cabin
	Type</td><td>Sleeps</td>
		<td width="80">Nightly Cost</td><td width="100">Assigned To</td>
	</tr>
	<tr class="claimed">
	<td>6	</td><td>2 bd</td><td>4-5</td><td>115.00
	</td><td><em>Sue</em>, Aixa, Allen</td>
	</tr>
	<tr class="claimed">
	<td>7	</td><td>Studio Deluxe</td><td>2 lovebirds</td><td>95.00</td><td>Bride and Groom (Aaron)</td>
	</tr>
	<tr>
	<td>8	</td><td>1 bd		</td><td>2-4</td><td>105.00 </td><td>Open?  Call for info.</td>
	</tr>
	<tr class="claimed">
	<td>14	</td><td>2 bd deluxe	</td><td>5-6</td><td>135.00
</td><td>Auntie Lorin (Aaron Deposit)</td>
	</tr>
	<tr class="claimed">
	<td>15	</td><td>2 bd deluxe w/loft</td><td>11-12</td><td>200.00</td><td>Naple Family</td>
	</tr>
	<tr class="claimed">
	<td>19</td><td>2 bd</td><td>4-5</td><td>115.00</td><td>Sherrie, Rich,
Mike, and Marie</td>
	</tr>
	<tr class="claimed">
	<td>20</td><td>2 bd</td><td>4-5</td><td>115.00</td><td>Aaron's Mom
(Diane Deposit)</td>
	</tr>
	<tr class="claimed">
	<td>21</td><td>2 bd</td><td>4-5</td><td>115.00</td><td>Grandpa Vern,
Uncle Matt and Boys 
(Aaron Deposit)</td>
	</tr>
	</table>
	<p class="margin4"><b><a href="contact.php">Contact us</a></b> to take
	an open cabin.  See map below:
	</p>

	<table class="it">
	<td> 
	<a href="crescent-lk-pamphlet.gif"><img src="crescent-lk-pamphlet-150.gif"></a>
	<br>
	<a href="crescent-lk-pamphlet.gif"> Pamphlet</a>
	</td>
	<td>
	<a href="crescent-lk-cabins.gif"><img src="crescent-lk-cabins-150.gif"></a>
	<br>
	<a href="crescent-lk-cabins.gif">Map of Cabins</a>
	</td>
	</tr>
	</table>

	<h2>Other Places to Stay</h2>
	<p>
	<ul>
	<li><b>Willamette Pass Inn</b>.  This is the closest option, about 1-2
	miles from Crescent Lake.  Looks decent.  Rooms are $78-108 per night.
	<a
	href="http://www.willamettepassinn.com">www.willamettepassinn.com</a>.
	(541)433-2211
	</li>
	<li><b>Crescent Lake Campground</b>.  Free camping, a short walk from
	the Crescent Lake Lodge!  First come, first served.  We'll try to grab
	some spots early by setting up tents, but no guarantees.
	</li>
	<li><b>Crescent Creek Cottages</b>. These are the next closest option
	after Willamette Pass Inn.  These look very rustic, not too fancy, and
	affordable.  We'll let you decide.  
	<a
	href="http://crescentcreekcottages.com/index.html">crescentcreekcottages.com</a>.
	(541)433-2324
	</li>
	<li><b>Shelter Cove Resort</b> offers cabins on another nearby lake.
	These are about 10 miles from Crescent Lake.  
	<a href="http://sheltercoveresort.com">sheltercoveresort.com</a>.
	(541)433-2548
	</li>
	</ul>
	For more options, <a href="contact.php">contact us</a>.
	</p>


	<hr class="dotted">
	<!-- begin talkback -->
	<table class="talkback">
	<tr><td class="title"><b>Talk Back!</b> &nbsp; &nbsp;
	<font size="-1">[add one <a href="lodging.php#_commento_">below</a>]
	</td>
		<tr><td>By <b>can't tell		</b>
		from <b>Planet Zoltar</b> on 2005-02-16 18:16:55.
		<br>
		<p> I can't wait, it will be great		</td></tr>
		<tr><td>By <b>Vern		</b>
		from <b>Carson City</b> on 2005-05-19 07:11:14.
		<br>
		<p> We think we can make it.  Sounds like a great opportunity to see family and have some fun.  Looking forward with baited breath.		</td></tr>
		<tr><td>By <b>Matt Grabow		</b>
		from <b>Reno Nv</b> on 2005-06-14 22:30:19.
		<br>
		<p> Aaron and Jocelyn, first off CONGRATS! Im definitly going to try my hardest to come celebrate such a happy occasion. I hope to see you in August. 
		</td></tr>
		<tr><td>By <b>Auntie NoNo		</b>
		from <b>Lethbridge, Alberta, Canada</b> on 2005-08-01 00:30:59.
		<br>
		<p> Aaron and Jocelyn,
Congrats! from me too!  I sent my R.S.V.P. a couple of weeks ago...but since the pony express here is run by "Arctic Turtles", you haven't received it yet.  I rsvp'd for 5, meaning myself, Afton, Matt, Mike and Mark.  I would like one		</td></tr>
	<tr><td>
	<form method="post" action="lodging.php">
	<a name="_commento_">
	<h3>Add a comment</h3>
	</a>
	Name:&nbsp;<input name="name" type="text" maxlength="48" size="20" />
	Location:&nbsp;<input name="location" type="text" maxlength="48" 
	    size="20" />
	<br>Comment:<br>
	<textarea name="comment" rows="6" cols="40" maxlength="255"> </textarea>
	<br>Spam Guard: Type the red letters only here 
<b>b<font color="#ee0000">ar</font>l<font color="#ee0000">z</font>k 
&nbsp; <input name="screen" type="text" maxlength="10" size="5">&nbsp;
	<input type="Submit" value="Submit"> 
	</form>
	</td></tr>
	</table>
	<!-- end talkback -->

   	</td></tr>
	</table>

	<p class="margin5">
	  <b>Questions?</b> <a href="contact.php">Contact</a> Aaron and Jocelyn.
	</p>


<p> &nbsp; </p>
<p> &nbsp; </p>

  </body>
</html>

