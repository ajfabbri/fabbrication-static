

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<title>
Aaron and Jocelyn :: The Wedding</title>
<LINK REL=StyleSheet HREF="wed-style.css" TYPE="text/css" MEDIA=screen TITLE=wed>
</head>

<body>
	<table width="800"  border="0" cellspacing="0" cellpadding="0">
		<tr><td width="800" colspan="2">
<img src="joc-aaron-sisters-hdr-800.jpg">			</td>
		</tr>
		<tr><td width="800" colspan="2">
				<img src="aj-shadow-800.png">
			</td>
		</tr>
		<tr><td width="800" bgcolor="#cccccc">

	<tr>
	<td class="navcontain">
	  <center>
		<table class="nav"><tr><td>
		  <a href="index.php">Home</a>
		  <a href="story.php">The Story</a>
		  <a href="../blog/exit.php?url_id=113&amp;entry_id=10">Engagement Party</a>
		  <a href="event.php">Main Event</a>
		  <a href="lodging.php">Lodging</a>
		  <a href="registry.php">Registry</a>
		  <a href="contact.php">Contact Us</a>
		  <a href="../main.php">Fabbrication</a>
		  </td></tr>
	   	</table>
	  <p class="counter">
		
1819	  </p>
	  <p class="gensmall">unique visitors since 10/12/2004</p>	 
	<p>&nbsp;</p>	
	<h4>Extra Links</h4>
	<p><a
href="http://www.fs.fed.us/r6/centraloregon/recreation/fishing/lake-reservoir/crescent.shtml">Crescent
Lake Info</a>
	<p><a href="http://tinyurl.com/cexow">Directions from Carson City, NV to
Crescent Lake, OR</a>
	  </center>
 	  </td>

	<td class="content">
<h1>The Wedding</h1>	<h2>Updated May 15, 2005</h2>


	<p class="margin1"> 
	Arrive on time (August 27, 3:30 pm) at the <a
	href="http://www.willamettepass.com">Willamette Pass Ski Resort</a>
	for a gondola ride to the summit, where the <b>ceremony</b> will
	take place (weather permitting). 
	</p>

	<p class="margin1"> 
	After the ceremony, we will ride back down the mountain to the
	<b>reception</b> at the lounge in the main lodge.
	</p>

	<p class="margin1"> 
	In addition to the main event at Willamette Pass Ski Resort, we will be
	visiting and vacationing at the Crescent Lake Resort through Monday
	morning.
	See the <a href="lodging.php">lodging</a> page for details.
	</p> 
	<p class="margin4">
	<a href="contact.php">Contact us</a> if you have questions.
	</p> 


   	</td></tr>
	</table>

	<p class="margin5">
	  <b>Questions?</b> <a href="contact.php">Contact</a> Aaron and Jocelyn.
	</p>


<p> &nbsp; </p>
<p> &nbsp; </p>

  </body>
</html>

