


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<title>
Aaron and Jocelyn :: Home</title>
<LINK REL=StyleSheet HREF="wed-style.css" TYPE="text/css" MEDIA=screen TITLE=wed>
</head>

<body>
	<table width="800"  border="0" cellspacing="0" cellpadding="0">
		<tr><td width="800" colspan="2">
<img src="joc-ring-summit-hdr-800.jpg">			</td>
		</tr>
		<tr><td width="800" colspan="2">
				<img src="aj-shadow-800.png">
			</td>
		</tr>
		<tr><td width="800" bgcolor="#cccccc">

	<tr>
	<td class="navcontain">
	  <center>
		<table class="nav"><tr><td>
		  <a href="index.php">Home</a>
		  <a href="story.php">The Story</a>
		  <a href="../blog/exit.php?url_id=113&amp;entry_id=10">Engagement Party</a>
		  <a href="event.php">Main Event</a>
		  <a href="lodging.php">Lodging</a>
		  <a href="registry.php">Registry</a>
		  <a href="contact.php">Contact Us</a>
		  <a href="../main.php">Fabbrication</a>
		  </td></tr>
	   	</table>
	  <p class="counter">
		
7035	  </p>
	  <p class="gensmall">unique visitors since 10/12/2004</p>	 
	<p>&nbsp;</p>	
	<h4>Extra Links</h4>
	<p><a
href="http://www.fs.fed.us/r6/centraloregon/recreation/fishing/lake-reservoir/crescent.shtml">Crescent
Lake Info</a>
	<p><a href="http://tinyurl.com/cexow">Directions from Carson City, NV to
Crescent Lake, OR</a>
	  </center>
 	  </td>

	<td class="content">
<h1>Home</h1>	<h2>Aaron Fabbri and Jocelyn Naple are Married!</h2>

	<hr class="dotted">
	<code>August 31, 2005</code>
	<p class="margin2">
	My friend and photographer Collin Andrew has posted a couple of pictures
from the wedding <a href="http://www.collinandrew.net">here</a>.  We're posting 
more pictures <a
href="http://beater.kicks-ass.net/gallery/wedding">here</a> as we get them.
</p>

	<hr class="dotted">
	<code>May 15, 2005</code>
	<h3>Ceremony and Reception Plans Updated</h3>
	<p class="margin2">
	See the <a href="event.php">Main Event</a> page for updated details
	on the ceremony and reception.
	</p>
	<hr class="dotted">
	<h3>Cabin Reservations Due </h3>
	<p class="margin2">
	We put down some deposits on March 10th.  Details on the 
	<a href="lodging.php">lodging</a> page.
	</p>


	<hr class="dotted">

	<h3>Save The Date!</h3>
	<h4>August 27, 2005</h4>
	<p style="margin">
	See the official Save the Date <a href="save-the-date.html">flyer</a>.  
	There's also a nice <a href="http://fabbrication.net/wedding/save-the-date.pdf">printable version</a> 
	<font size="-1">(requires 
	<a
	href="http://www.adobe.com/products/acrobat/readstep2.html">Acrobat
	Reader</a>)</font>
	</p>

	<p>&nbsp;</p>
	<hr class="dotted">

	<p style="margin2">
	<b>Read </b><a href="story.php">the story</a>
	of when Aaron proposed to Jocelyn.
	</p>
	<hr class="dotted">
	<!-- begin talkback -->
	<table class="talkback">
	<tr><td class="title"><b>Talk Back!</b> &nbsp; &nbsp;
	<font size="-1">[add one <a href="index.php#_commento_">below</a>]
	</td>
		<tr><td>By <b>Aaron		</b>
		from <b>Seattle</b> on 2004-11-13 16:58:00.
		<br>
		<p>Can't wait until August next year.  It's going to be a fun party!		</td></tr>
		<tr><td>By <b>Sammy		</b>
		from <b>Seattle</b> on 2004-11-13 17:17:43.
		<br>
		<p>Meeeow.  Me, meow.		</td></tr>
		<tr><td>By <b>aaron		</b>
		from <b>new york</b> on 2004-12-11 18:10:19.
		<br>
		<p>Hi from new york city! 		</td></tr>
		<tr><td>By <b>mamalinda		</b>
		from <b>amsterdam</b> on 2004-12-14 08:59:33.
		<br>
		<p> miss you  guys  even sammy!!!!!!!!!!		</td></tr>
		<tr><td>By <b>Emma		</b>
		from <b>Eugene</b> on 2005-02-06 18:13:08.
		<br>
		<p>Scuttlebutt and casbah cruisin' down the road
Aaron and the Juice singin' go dog go
HOT DOG!		</td></tr>
		<tr><td>By <b>Farty MGee		</b>
		from <b>Fartstown USA</b> on 2005-08-18 17:52:07.
		<br>
		<p> I Love Farts!!!!!!		</td></tr>
		<tr><td>By <b>I_See_Teeth		</b>
		from <b>Cleveland</b> on 2005-08-24 15:21:11.
		<br>
		<p> Nice teeth!!		</td></tr>
		<tr><td>By <b>Sarah		</b>
		from <b>Eugene</b> on 2005-08-24 15:22:40.
		<br>
		<p> Congrats, guys! Now hurry up and make my kids some cousins, dangit!		</td></tr>
		<tr><td>By <b>Geneva		</b>
		from <b>New Mexico</b> on 2005-09-12 11:50:11.
		<br>
		<p> awesome wedding pics..oh my gosh'' i was speechless and felt a tear. beautiful thank you for sharing your pics.. stop at New Mexico say hi.. love u guys..		</td></tr>
		<tr><td>By <b>Mammadiane		</b>
		from <b>Eugene, OR</b> on 2005-11-15 21:42:44.
		<br>
		<p> Hey! hows about some current blogging from Firenza?! Love you two....Mrreeeoooowwww myaaa mrrrr, says Sammy!		</td></tr>
		<tr><td>By <b>Anton		</b>
		from <b>united states</b> on 2006-03-31 22:08:15.
		<br>
		<p>I want mp3 player. What will advise?		</td></tr>
		<tr><td>By <b>Dmitry		</b>
		from <b>united states</b> on 2006-04-05 01:41:04.
		<br>
		<p>Hi
To write the letter, it is necessary ...		</td></tr>
	<tr><td>
	<form method="post" action="index.php">
	<a name="_commento_">
	<h3>Add a comment</h3>
	</a>
	Name:&nbsp;<input name="name" type="text" maxlength="48" size="20" />
	Location:&nbsp;<input name="location" type="text" maxlength="48" 
	    size="20" />
	<br>Comment:<br>
	<textarea name="comment" rows="6" cols="40" maxlength="255"> </textarea>
	<br>Spam Guard: Type the red letters only here 
<b>b<font color="#ee0000">ar</font>l<font color="#ee0000">z</font>k 
&nbsp; <input name="screen" type="text" maxlength="10" size="5">&nbsp;
	<input type="Submit" value="Submit"> 
	</form>
	</td></tr>
	</table>
	<!-- end talkback -->

   	</td></tr>
	</table>

	<p class="margin5">
	  <b>Questions?</b> <a href="contact.php">Contact</a> Aaron and Jocelyn.
	</p>


<p> &nbsp; </p>
<p> &nbsp; </p>

  </body>
</html>

