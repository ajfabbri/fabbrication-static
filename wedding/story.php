

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<title>
Aaron and Jocelyn :: The Story</title>
<LINK REL=StyleSheet HREF="wed-style.css" TYPE="text/css" MEDIA=screen TITLE=wed>
</head>

<body>
	<table width="800"  border="0" cellspacing="0" cellpadding="0">
		<tr><td width="800" colspan="2">
<img src="sisters-quotes-800.jpg">			</td>
		</tr>
		<tr><td width="800" colspan="2">
				<img src="aj-shadow-800.png">
			</td>
		</tr>
		<tr><td width="800" bgcolor="#cccccc">

	<tr>
	<td class="navcontain">
	  <center>
		<table class="nav"><tr><td>
		  <a href="index.php">Home</a>
		  <a href="story.php">The Story</a>
		  <a href="../blog/exit.php?url_id=113&amp;entry_id=10">Engagement Party</a>
		  <a href="event.php">Main Event</a>
		  <a href="lodging.php">Lodging</a>
		  <a href="registry.php">Registry</a>
		  <a href="contact.php">Contact Us</a>
		  <a href="../main.php">Fabbrication</a>
		  </td></tr>
	   	</table>
	  <p class="counter">
		
2075	  </p>
	  <p class="gensmall">unique visitors since 10/12/2004</p>	 
	<p>&nbsp;</p>	
	<h4>Extra Links</h4>
	<p><a
href="http://www.fs.fed.us/r6/centraloregon/recreation/fishing/lake-reservoir/crescent.shtml">Crescent
Lake Info</a>
	<p><a href="http://tinyurl.com/cexow">Directions from Carson City, NV to
Crescent Lake, OR</a>
	  </center>
 	  </td>

	<td class="content">
<h1>The Story</h1><h3>When I Proposed to Jocelyn</h3>
<h4>By Aaron Fabbri</h4>
<p class="story">
<font size="+1">On the weekend of August 15th (my birthday), 2004, Jocelyn and
I went backpacking in the Three Sisters Wilderness in Oregon.
</p>

<p class="story">
We met some of my friends from Eugene, Oregon at the trailhead late Friday
night. They greeted me with a hostess cupcake and sang happy birthday.  We
camped out
and then hiked south into the wilderness the next morning.  The first day we
went 12 miles and
found a nice ridge to camp on near the middle of three big volcanos; the Middle
Sister.  The next day we climbed the Middle Sister which was 3 hours of hard
work up and 1 1/2 hours back down.   The views were amazing and it was a little
scary.
</p>

<p class="story">
All the way up to the summit of the Middle Sister I was carrying the pack with
about a gallon of water in it; I was the slowest hiker.  I was also carrying a
ring, hidden in a box in a box in a black sock, in my jacket pocket.
</p>

<p class="story">
At the top of the summit we ate some snacks and drank water.  I went over to the
other side of the summit area and asked Jocelyn to come and look at the view.
My friends all knew that I was going to propose, but Jocelyn did not. 
I put my arms around Jocelyn from behind as we both soaked up the amazing views.
She turned and asked me, "Why is your heart beating so fast?".  I said it was
the altitude and the tricky ascent.  Soon she knew the real reason.  
I told Jocelyn I have a gift for her, and started fumbling for the ring which
was in a box inside of a box in a black sock in my pocket.  I gave the box to
her, at which point she probably knew what was up.  With some encouragement, I
got down on one knee and asked her to marry me.   It wasn't the best delivery on
my part, but with the scenery it turned out to be a very memorable moment.
</p>

<p class="story">
This was turning out to be a very eventful birthday for me, and I got what I
wished for; to marry Jocelyn.  A couple of years ago I'd be the last person on
earth to think I'd be getting married.  All I can say is that you know when you
meet the right person.  I'm reminded daily what a great catch she is.  We have a
lot of fun together, <em>but lets get back to the story</em>.
</p>

<p class="story">
The descent was <em>so</em> much easier than the ascent.  The loose rocks and
cinder, which impeded progress on the way up, made for a fun slide back down.
We stopped at a beautiful bright blue tarn (small glacial lake) about 1000 ft.
down from the summit and drank water
straight from it.  When we arrived back at our camp at Arrowhead Lake, we 
 celebrated with some wine I packed in.  What a great day it was.  That night we
packed up and moved to another nearby camping spot at the Obsidian Springs.  
The Obsidian Springs are some amazing springs which came out of the side of the
mountain (glacial melt).  The water flows over volcanic obsidian glass and was
very cold, and good to drink.  As the sun went down, the obsidian on the ground
sparkled like broken glass.
</p>

<p class="story">
The next morning, Jocelyn and I parted ways with my friends and hiked back North
towards the car (11 miles) and stayed at some more beautiful lakes.  </p>

<p class="story">
Pictures from this eventful weekend are at: <br>

<a href="http://beater.kicks-ass.net/photo/seattle2/2004_08_15/index.html">
http://beater.kicks-ass.net/photo/seattle2/2004_08_15/index.html</a>

</p>


   	</td></tr>
	</table>

	<p class="margin5">
	  <b>Questions?</b> <a href="contact.php">Contact</a> Aaron and Jocelyn.
	</p>


<p> &nbsp; </p>
<p> &nbsp; </p>

  </body>
</html>

