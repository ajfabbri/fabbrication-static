

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head profile="http://www.w3.org/2005/10/profile">
    <link rel="icon" type="image/png" href="../favicon2.png">
    <LINK REL=StyleSheet HREF="../styles/fab-pro.css" 
	  TYPE="text/css" MEDIA=screen TITLE=fab>
    <title>
A. Fabbri :: Projects    </title>
  </head>

  <body>
	<table class="tblmain">
	<tr>
	  <td class="maintop-left">
Projects   	  </td>
	  <td class="maintop">
	    <img src="aaron-j-fabbri-300x80.gif" alt="Aaron J Fabbri">
   	  </td>
	</tr>

	<tr>
	  <td class="navcontain">
	  <center>
		<table class="nav"><tr><td>
		  <a href="index.php">Home</a>
		  <a href="resume.php">Resume</a>
		  <a href="projects.php">Projects</a>
		  <a href="bb/index.html">Technical Forums</a>
		  <a href="contact.php">Contact</a>
		  </td></tr>
	   	</table>
	  <p class="counter">
		2046	  </p>
	  <p class="gensmall">unique visitors since 10/10/2004</p>	 
	  </center>
 	  </td>

	<td class="content">

	<table class="plist">

	<tr><td>
	<p class="projname">
	<a href="project/GraphApp/index.html">Graph Editing and Drawing Tool</a>
	</p>
	<p class="warning">01-22-2005 - Posted version 0.1. </p>
	<p class="projdesc">GraphApp provides a GUI for editing graphs, along
with a framework for generating and drawing graphs. 
	<img src="project/GraphApp/graph-app-sshot-0-1.png" align="right"><br>
	(written in Java)
	</p>
	</td></tr>

	<tr><td>
	<p class="projname">
	<a href="../blog/exit.php?url_id=179&amp;entry_id=23">MP3 Downsampler</a>
	</p>

	<p class="warning">Posted version 0.2.  Click link above.</p>
	<p class="projdesc">
	The MP3 Downsampler creates a copy of your entire .mp3 music collection,
	re-encoding the files at a lower bitrate.  This allows you to encode
	your CD's at a high quality setting for archival purposes, and then also
	create lower-quality versions so you can fit more songs on your
	portable mp3 player.  (written in Python)
	</p>
	</td></tr>
	<tr><td>

	<p class="projname">
	Super Registry	
	</p>

	<p class="projdesc">
	The Super Registry is a web-based gift registry. Super Registry 
	brings together
	items sold at stores which do not offer their own registry service.  I
	threw this together for my recent 
	<a href="http://www.fabbrication.net/wedding">wedding</a>.
	</p>
	</td></tr>
<php
	/* TODO:
	 * Clean up wedding/registry.php, copy to pro/repository, link into 
	 * this page.
	 */
	gen_tail($title);

?>
	
