
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head profile="http://www.w3.org/2005/10/profile">
    <link rel="icon" type="image/png" href="../../../../favicon2.png">
    <LINK REL=StyleSheet HREF="../../../../styles/fab-pro.css" 
	  TYPE="text/css" MEDIA=screen TITLE=fab>
    <title>
A. Fabbri :: GraphApp Launcher    </title>
  </head>

  <body>
	<table class="tblmain">
	<tr>
	  <td class="maintop-left">
GraphApp Launcher   	  </td>
	  <td class="maintop">
	    <img src="../../../aaron-j-fabbri-300x80.gif" alt="Aaron J Fabbri">
   	  </td>
	</tr>

	<tr>
	  <td class="navcontain">
	  <center>
		<table class="nav"><tr><td>
		  <a href="../../../index.php">Home</a>
		  <a href="../../../resume.php">Resume</a>
		  <a href="../../../projects.php">Projects</a>
		  <a href="../../../bb/index.html">Technical Forums</a>
		  <a href="../../../contact.php">Contact</a>
		  </td></tr>
	   	</table>
	  <p class="counter">
		662	  </p>
	  <p class="gensmall">unique visitors since 10/10/2004</p>	 
	  </center>
 	  </td>

	<td class="content">
	<p class="projname">
	Graph Editing and Drawing Tool applet launcher 	
	</p>

	<p class="warning">01-22-2005 - Posted version 0.1. </p>

	<h3>GraphApp 0.1 Applet</h3>

<p align=center>
<applet archive="graph.jar" code="http://fabbrication.net/pro/project/GraphApp/GraphApp-0.1/net/fabbrication/graph/GraphApplet.class" width=250 height=40
	codebase="/pro/project/GraphApp/GraphApp-0.1/"
           alt="(Applet 'GraphApplet' should be displayed here.)">
     <font color="#E70000">
     (Applet "GraphApplet" would be displayed here<br>
     if you had Java installed correctly.)</font>
</applet>
</p>

	<h3>Some things to try</h3>
	<ul>
	<li>Click to create vertices.  Click on a vertex to highlight it and to
unhighlight it.
	<li>When a vertex is highlighted, clicking on another allows you to
create an edge.
	<li>When a vertex is highlighted, pressing the Delete key will remove
it.
	<li>A tree generation algorithm is available in the Generate menu.</li>

	<li>Save and Load Graph functions are not yet implemented.
</ul>
	
   	</td></tr>
	</table>

	<p class="margin5">
	  <b>Questions?</b> <a href="http://fabbrication.net/pro/project/GraphApp/GraphApp-0.1/contact.php">Contact</a> Aaron.
	</p>
	<p><small>
		  <a href="../../../../systems/index.php">Fabbri Systems</a>
		  <a href="../../../../main.php">Fabbrication</a>
		  </small>
		  </p>


<p> &nbsp; </p>
<p> &nbsp; </p>
<!-- google analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4159952-2");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<!-- END google analytics -->

  </body>
</html>

	
