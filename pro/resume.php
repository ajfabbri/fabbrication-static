

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head profile="http://www.w3.org/2005/10/profile">
    <link rel="icon" type="image/png" href="../favicon2.png">
    <LINK REL=StyleSheet HREF="../styles/fab-pro.css" 
	  TYPE="text/css" MEDIA=screen TITLE=fab>
    <title>
A. Fabbri :: Resume    </title>
	  	<link href="liresume/aaron-fabbri-li_files/slice.css" media="all" rel="stylesheet" type="text/css" class="resume-style">
  </head>

  <body>
	<table class="tblmain">
	<tr>
	  <td class="maintop-left">
Resume   	  </td>
	  <td class="maintop">
	    <img src="aaron-j-fabbri-300x80.gif" alt="Aaron J Fabbri">
   	  </td>
	</tr>

	<tr>
	  <td class="navcontain">
	  <center>
		<table class="nav"><tr><td>
		  <a href="index.php">Home</a>
		  <a href="resume.php">Resume</a>
		  <a href="projects.php">Projects</a>
		  <a href="bb/index.html">Technical Forums</a>
		  <a href="contact.php">Contact</a>
		  </td></tr>
	   	</table>
	  <p class="counter">
		5103	  </p>
	  <p class="gensmall">unique visitors since 10/10/2004</p>	 
	  </center>
 	  </td>

	<td class="content">

<p class="margin2" style="font-family: Verdana, Arial, sans-serif">Also
available in <a
href="aaron-fabbri-resume-2011.pdf">pdf.</a>
</p>

<hr> 
<!-- begin paste -->
  <div id="resume">
	  <div class="header"></div>
		      
  <div class="section picture">
    <div class="content"><img alt="Profile Picture" src="liresume/aaron-fabbri-li_files/0_VoW0fTJ5JUSTH35XVdZufGJ5UxVTokqX4mg8fisMkJalzhikn2M1T_aBvtsYW_zHRdw3hkXuaBwf">
</div>
    <div class="clear"></div>
  </div>

		      	<h1 class="name">
				<span class="first-name">Aaron</span>

				<span class="last-name">Fabbri</span>

				<span class="headline">Technical Lead</span>

		<div class="clear"></div>	
	</h1>

		      
  <div class="section contact">
      <h2 class="section-header">
	<span class="text">Contact</span>	
</h2>
    <div class="content">    	<div class="email-address">
		<span class="label">Email:</span>
		<span class="content">
		ajfabbri (@)  gmail (.) com</span>
	</div>




</div>
    <div class="clear"></div>
  </div>

		      
  <div class="section summary">
      <h2 class="section-header">
	<span class="text">Summary</span>	
</h2>
    <div class="content"><p>Senior software engineer specializing in Networks / Distributed Systems, Filesystems, and Security.</p>
</div>
    <div class="clear"></div>
  </div>

		      
  <div class="section specialties">
      <h2 class="section-header">
	<span class="text">Specialties</span>	
</h2>
    <div class="content"><p>Software development lead and project management. Performance-critical code. Low latency messaging (HFT etc.).  FreeBSD and Linux kernel/user development.  Scalable, fault-tolerant systems, network protocol design and implementation.  Web/enterprise security and performance.</p>
</div>
    <div class="clear"></div>
  </div>

		      

		      
  <div class="section positions">
      <h2 class="section-header">
	<span class="text">Experience</span>	
</h2>
    <div class="content">	  <div class="position first">
	      	<div class="company-name">Cisco Systems</div>


  	<div class="dates">
	    <span class="start-month">06 /</span>
			<span class="start-year">2006</span>
  			<span class="date-separator">-</span>
				<span class="is-current">Present</span>
  	</div>

	<div class="title">Technical Lead</div>
						
	<div class="job-summary"><ul>
  <li>
    <p>Lead developer for low latency, kernel-bypass drivers for the Cisco Virtual Interface Card (VIC), a 10 GbE adapter which supports both hypervisor and kernel bypass.</p>
  </li>
  <li>
    <p>Lead architect and developer of Cisco Datagram Acceleration Layer (DAL), an ultra low latency (ULL), kernel-bypass UDP/IP network stack which accelerates unicast and multicast traffic over InfiniBand and 10 Gig Ethernet networks.</p>
  </li>
  <li>
    <p>Wrote hardware requirements for OS-bypass and worked with multiple 10GbE NIC vendors on next generation silicon features.</p>
  </li>
  <li>
    <p>Accelerated performance of top market data middleware (Tibco RV, 29West LBM, Wombat, Reuters
RMDS, MRG) with or without help of the ISVs. Google “Cisco DAL” for press releases.</p>
  </li>
</ul>
</div>
							

	  </div>
	  <div class="position">
	      	<div class="company-name">isilon systems</div>


  	<div class="dates">
			<span class="start-year">2001</span>
  			<span class="date-separator">-</span>
				<span class="end-year">2005</span>
  	</div>

	<div class="title">software developer</div>
						
	<div class="job-summary"><ul>
  <li>
    <p>Primary developer of new InfiniBand (IB) network stack based on FreeBSD and Linux. Project was a success:  We were the first clustered storage system to ship an IB back end,  outperforming Gigabit Ethernet and reducing host utilization.  Implemented seamless fail-over and a number of kernel optimizations.</p>
  </li>
  <li>
    <p>Developed high-performance distributed filesystem code and modifications to a well-known open source kernel.  Responsible for key early filesystem features such as multiple drive support. </p>
  </li>
  <li>
    <p>Increased filesystem and NFS server performance by analyzing newtork traces and kernel profiles, redesigning distributed algorithms, and modifying server code.</p>
  </li>
  <li>
    <p>Modified network and disk controller drivers to add features such as host bus error detection and sector remapping support.</p>
  </li>
  <li>
    <p>Turned a disaster scenario into a top repeat account by designing and executing a delicate data recovery operation.</p>
  </li>
</ul>
</div>
							

	  </div>
	  <div class="position">
	      	<div class="company-name">University of Oregon</div>


  	<div class="dates">
	    <span class="start-month">04 /</span>
			<span class="start-year">2000</span>
  			<span class="date-separator">-</span>
	      <span class="end-month">06 /</span>
				<span class="end-year">2001</span>
  	</div>

	<div class="title">Graduate Research Fellow</div>
						
	<div class="job-summary"><ul>
  <li>
    <p>Increased performance of content distribution networks using Linux, IP Multicast, C/C++, TCL, and Perl.</p>
  </li>
  <li>
    <p>Designed and implemented multicast routing protocols and wrote protocol specifications.  Wrote automated test tools, evaluated performance, and presented results.</p>
  </li>
  <li>
    <p>Designed and programmed efficient network simulations, processed data, and plotted graphs.</p>
  </li>
  <li>
    <p>Co-authored two original research papers and attended networking conferences and workshops.</p>
  </li>
  <li>
    <p>Designed and built a multicast-enabled Linux cluster for testing network routing protocols.</p>
  </li>
</ul>
</div>
							

	  </div>
	  <div class="position">
	      	<div class="company-name">Enivironmental Protection Agency</div>


  	<div class="dates">
	    <span class="start-month">06 /</span>
			<span class="start-year">1999</span>
  			<span class="date-separator">-</span>
	      <span class="end-month">09 /</span>
				<span class="end-year">1999</span>
  	</div>

	<div class="title">Software Engineer</div>
						
	<div class="job-summary"><ul>
  <li>
    <p>Developed scientific computation and data acquisition software using C, Java, Perl, SQL, and TCP/IP.</p>
  </li>
  <li>
    <p>Implemented, tested, documented, and deployed application-layer protocol for collecting environmental sensor data.</p>
  </li>
  <li>
    <p>Supported research staff as a contractor to the U.S. Environmental Protection Agency (EPA).</p>
  </li>
  <li>
    <p>Administered Oracle database system including performance tuning, backup and restore, reporting, and UNIX  Solaris and Linux administration.</p>
  </li>
</ul>
</div>
							

	  </div>
	  <div class="position">
	      	<div class="company-name">Entertainment Data Solutions</div>


  	<div class="dates">
	    <span class="start-month">01 /</span>
			<span class="start-year">1998</span>
  			<span class="date-separator">-</span>
	      <span class="end-month">06 /</span>
				<span class="end-year">1999</span>
  	</div>

	<div class="title">Senior Software Engineer</div>
						
	<div class="job-summary"><ul>
  <li>
    <p>Designed and implemented multi-user database-driven point of sale and inventory management software using (Java, SQL).</p>
  </li>
  <li>
    <p>Developed Internet messaging client and server to reduce customer support costs and improve response times.</p>
  </li>
  <li>
    <p>Installed and tested Ethernet networks, servers and clients at customers’ sites.</p>
  </li>
  <li>
    <p>Designed and built company network and services; Routers, Switches,
Linux, Windows NT, Apache, Sendmail, CGI, etc…</p>
  </li>
</ul>
</div>
							

	  </div>
</div>
    <div class="clear"></div>
  </div>

		      
  <div class="section educations">
      <h2 class="section-header">
	<span class="text">Education</span>	
</h2>
    <div class="content">	  <div class="education first">
	      	<div class="school-name">University of Oregon</div>


  	<div class="dates">
			<span class="start-year">1999</span>
  			<span class="date-separator">-</span>
				<span class="end-year">2001</span>
  	</div>

<div class="program">
		<span class="degree">MS</span>

		<span class="program-separator">,</span>
		<span class="field-of-study">Distributed Systems</span>
	
</div>	
					
	  </div>
	  <div class="education">
	      	<div class="school-name">University of Oregon</div>


  	<div class="dates">
			<span class="start-year">1993</span>
  			<span class="date-separator">-</span>
				<span class="end-year">1997</span>
  	</div>

<div class="program">
		<span class="degree">BS</span>

		<span class="program-separator">,</span>
		<span class="field-of-study">Computer and Information Science</span>
	
</div>	
					
	  </div>
</div>
    <div class="clear"></div>
  </div>

		      
  <div class="section honors">
      <h2 class="section-header">
	<span class="text">Honors</span>	
</h2>
    <div class="content"><p>Cisco Individual Acheivement Award, September 2007</p>
</div>
    <div class="clear"></div>
  </div>

<!--end paste -->
<hr>
</body>

</html>


   	</td></tr>
	</table>

	<p class="margin5">
	  <b>Questions?</b> <a href="contact.php">Contact</a> Aaron.
	</p>
	<p><small>
		  <a href="../systems/index.php">Fabbri Systems</a>
		  <a href="../main.php">Fabbrication</a>
		  </small>
		  </p>


<p> &nbsp; </p>
<p> &nbsp; </p>
<!-- google analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4159952-2");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<!-- END google analytics -->

  </body>
</html>

