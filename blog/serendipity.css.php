img {
   behavior: url("plugin/pngbehavior.htc");
}
img {
   behavior: url("plugin/pngbehavior.htc");
}
img {
   behavior: url("plugin/pngbehavior.htc");
}
img {
   behavior: url("plugin/pngbehavior.htc");
}
img {
   behavior: url("plugin/pngbehavior.htc");
}
img {
   behavior: url("plugin/pngbehavior.htc");
}
img {
   behavior: url("plugin/pngbehavior.htc");
}
.serendipity_karmaVoting {
    text-align: center;
    font-size: 7pt;
    margin: 0px;
}

.serendipity_karmaVoting a {
    font-size: 7pt;
    text-decoration: none;
}

.serendipity_karmaVoting a:hover {
    color: green;
}

.serendipity_karmaError {
    color: #FF8000;
}
.serendipity_karmaSuccess {
    color: green;
}
.serendipity_karmaVoting_links,
.serendipity_karmaVoting_links a:hover,
.serendipity_karmaVoting_current-rating {
    background: url(plugins/serendipity_event_karma/img/stars-def-yellow-green-md.png) left;
    font-size: 0;
}
.serendipity_karmaVoting_links {
    position: relative;
    width: 100px;
    height: 20px;
    overflow: hidden;
    list-style: none;
    margin: 0px auto;
    padding: 0px;
    background-position: left top;     
    text-align: center;
}
.serendipity_karmaVoting_links li {
   display: inline; 
}
.serendipity_karmaVoting_links a ,
.serendipity_karmaVoting_current-rating {
    position:absolute;
    top: 0px;
    left: 0px;
    text-indent: -9000em;
    height: 20px;
    line-height: 20px;
    outline: none;
    overflow: hidden;
    border: none;
}
.serendipity_karmaVoting_links a:hover {
    background-position: left bottom;
}
.serendipity_karmaVoting_links a.serendipity_karmaVoting_link1 {
    width: 20%;
    z-index: 6;
}
.serendipity_karmaVoting_links a.serendipity_karmaVoting_link2 {
    width: 40%;
    z-index: 5;
}
.serendipity_karmaVoting_links a.serendipity_karmaVoting_link3 {
    width: 60%;
    z-index: 4;
}
.serendipity_karmaVoting_links a.serendipity_karmaVoting_link4 {
    width: 80%;
    z-index: 3;
}
.serendipity_karmaVoting_links a.serendipity_karmaVoting_link5 {
  width: 100%;
    z-index: 2;
}
.serendipity_karmaVoting_links .serendipity_karmaVoting_current-rating {
    z-index: 1;
    background-position: left center;
}

      .serendipity_findmore {
        margin: 5px auto 5px auto;
        padding: 5px;
        text-align: center;
      }

      .serendipity_findmore img {
        border: 0px;
      }

      .serendipity_diggcount {
          float: left;
      }
/* templates/blue/style.css  */
a, a:visited {
  color: #993300;
  text-decoration: none;
}

a:hover {
  color: #cc3300;
  text-decoration: underline;
}

body {
  background-color: #f0f0f0;
  color: #000000;
  font-family: sans-serif;
  line-height: 20px;
  margin-left: 0px;
  margin-right: 0px;
  margin-top: 0px;
  margin-bottom: 0px;
}

p, td, th, div, span {
  vertical-align: top;
}

#content {
  width: auto;
}

#mainpane {
  width: 100%;
}

#serendipity_banner {
  background-color: #dddddd;
  border-bottom: dashed 1px #000080;
  width: 100%;
}

a.homelink1,
a.homelink1:hover,
a.homelink1:link,
a.homelink1:visited,
#serendipity_banner h1 {
  color: #666666;
  font-size: x-large;
  font-weight: bold;
  margin-top: 0px;
  padding-left: 20px;
  padding-top: 5px;
  text-decoration: none;
}

a.homelink2,
a.homelink2:hover,
a.homelink2:link,
a.homelink2:visited,
#serendipity_banner h2 {
  color: #aaaaaa;
  font-size: large;
  font-weight: bold;
  padding-left: 20px;
  text-decoration: none;
}

#serendipityRightSideBar {
  background-color: #f0f0f0;
  border-left: 1px dashed #4068ab;
  border-bottom: 1px dashed #4068ab;
  padding: 20px;
  vertical-align: top;
}

#serendipityLeftSideBar {
  background-color: #f0f0f0;
  border-right: 1px dashed #4068ab;
  border-bottom: 1px dashed #4068ab;
  padding: 20px;
  vertical-align: top;
}

div.serendipitySideBarTitle {
  font-size: small;
  font-weight: bold;
  margin-bottom: 8px;
}

div.serendipitySideBarItem {
  background-color: #e6eaee;
  border: 1px dashed #d0d0d0;
  font-size: x-small;
  margin-bottom: 12px;
  padding-left: 10px;
  padding-right: 10px;
  padding-bottom: 10px;
}

.serendipity_entry {
  background-color: #e6eaee;
  display: block;
  padding-left: 20px;
  padding-right: 20px;
  padding-bottom: 10px;
}

div.serendipity_Entry_Date {
  background-color: #e6eaee;
  border: 1px dashed #d0d0d0;
  margin: 10px;
  padding: 10px;
  width: auto;
}

img.serendipity_entryIcon {
    float: right;
    border: 0px;
}

.serendipity_title {
  border: 1px;
  color: #1a3c5a;
  font-weight: bold;
  margin-bottom: 8px;
  margin-top: 8px;
  padding-left: 20px;
}

.serendipity_title a:link,
.serendipity_title a:visited {
  border: 0;
  color: #000000;
  text-decoration: none;
}

.serendipity_title a:hover {
  color: #cc3300;
}

.serendipity_date {
  color: #000080;
  display: block;
  font-weight: bold;
  text-align: right;
  width: 100%;
}

.serendipity_commentsTitle {
  background-color: #f2f4f6;
  border: 0px;
  color: #404040;
  display: block;
  font-size: small;
  padding-left:8px;
  width: 100%;
}

.serendipity_time {
  display: block;
  font-size: large;
  font-weight: bold;
  margin-top: 8px;
}

td.serendipity_commentsLabel {
  font-size: small;
  font-weight: bold;
  vertical-align: top;
  width: 10%;
}

td.serendipity_comment {
  color: #404040;
  font-size: small;
  margin-bottom: 12px;
  padding-left:8px;
}

.serendipity_comment_source {
  margin-top: 5px;
}

.serendipity_comment {
  padding-top: 1em;
  overflow: auto;
}

td.serendipityEntriesBox {
  background-color: #ffffff;
  padding: 10px;
  margin: 10px;
}

td.serendipity_admin {
  padding: 10px;
}

table.serendipity_calendar td {
  font-size: small;
  padding: 3px;
}

table.serendipity_calendar a {
  font-weight: bold;
  text-decoration:none;
}

table.serendipity_calendar a:hover {
  text-decoration:underline;
}

td.serendipity_weekDayName {
  background-color: #dddddd;
  font-size: small;
  font-weight: bold;
}

div.serendipityPlug, div.serendipityPlug a {
  font-size: small;
}

img.serendipityImageButton {
  cursor: hand;
}

div.serendipity_admin_title {
  font-size: large;
  font-weight: bold;
  margin-bottom: 12px;
}

div.serendipity_admin_list_title {
  font-weight: bold;
  margin-bottom: 8px;
}

td.serendipity_admin_list_item {
  border: dotted 1px #d0d0d0;
}

div.serendipity_entryFooter {
  clear: both;
  color: #000000;
  font-size: x-small;
  padding-top: 5px;
  padding-bottom: 4px;
}


/** Embedded images with the s9y image manager **/
.serendipity_imageComment_center,
.serendipity_imageComment_left,
.serendipity_imageComment_right {
    border: 1px solid black;
    background-color: #EFEFEF;
    margin: 3px;
}

.serendipity_imageComment_center {
    margin: 0px;
}

.serendipity_imageComment_left {
    float: left;
}

.serendipity_imageComment_right {
    float: right;
}

.serendipity_imageComment_img,
.serendipity_imageComment_img img {
    margin: 0px;
    padding: 0px;
    border: 0px;
    text-align: center;
}

.serendipity_imageComment_txt {
    margin: 0px;
    padding: 3px;
    clear: both;
    font-size: 9pt;
    text-align: center;
}

.serendipity_admin_list_item_even {
    background-color: #ffffff;
}

.serendipity_admin_list_item_uneven {
    background-color: #E0E0E0;
}

.serendipity_admin_filters {
    border: 1px dashed;
    background-color: #FFFFFF;
    font-size: 10px;
    margin-bottom: 10px;
    padding: 2px;
}

.serendipity_admin_filters_headline {
    border-bottom: 1px solid;
    font-weight: bold;
}

.serendipity_admin_sortorder {
    font-size: 10px;
    text-align: center;
}

.serendipity_admin_sortorder input,
.serendipity_admin_sortorder select,
.serendipity_admin_filters input,
.serendipity_admin_filters select {
    font-size: 10px;
}

.serendipity_comments {
}

.serendipity_center {
    margin-left: auto;
    margin-right: auto;
    text-align: center;
}

.serendipity_msg_important {
    color: red;
}

.serendipity_msg_notice {
    color: green;
}

.serendipity_entry_author_self {
}

.serendipity_comment_author_self {
    background-color: #f2f4f6;
}


/*** Additional plugin styles needed for the modernized output code ***/
/*** added by Matthias 'YellowLed' Mees                             ***/

.container_serendipity_archives_plugin ul,
.container_serendipity_syndication_plugin ul,
.container_serendipity_authors_plugin ul,
.container_serendipity_plugin_entrylinks ul {
    list-style: none;
    margin-left: 0;
    padding-left: 0;
}

.container_serendipity_authors_plugin .serendipitySideBarContent div {
    margin: 5px 0;
    padding-left: 0;
}

.serendipity_history_author { font-weight: bolder; }

.serendipity_history_intro,
.serendipity_history_info { margin-bottom: 3px; }

.serendipity_history_body { margin-bottom: 10px; }

.serendipity_recententries_entrylink { margin-bottom: 2px; }

.serendipity_recententries_entrydate { margin: 0 0 4px 0; }

.plugin_comment_body { padding: 3px 0; }

.serendipity_shoutbox_date { margin: 5px 0 2px 0; }

.serendipity_shoutbox_comment { padding: 3px 0; }

/*** End Additional plugin styles                                   ***/
