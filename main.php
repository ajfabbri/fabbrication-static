
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head profile="http://www.w3.org/2005/10/profile">
    <link rel="icon" type="image/png" href="favicon2.png">
    <LINK REL=StyleSheet HREF="styles/fab.css" 
	  TYPE="text/css" MEDIA=screen TITLE=fab>
    <title>
fabbrication.net :: Home    </title>

  </head>

  <body>
	<table class="tblmain">
	<tr>
	  <td class="maintop-left">
Home   	  </td>
	  <td class="maintop">
	    <img src="img/fabbrication-net-300x80.jpg" alt="fabbrication.net">
   	  </td>
	</tr>

	<tr>
	  <td class="navcontain">
	  <center>
		<table class="nav"><tr><td>
		  <a href="main.php">Home</a>
		  <a href="blog/blog.php">Blog</a>
		  <a href="pro/index.php">Professional</a>
		  <a href="wedding/index.php">Wedding</a>
		  <a href="photos.php">Photos</a>
		  <a href="links.php">Links</a>
		  <a href="homebrew_cheesemaking_supplies.php">Homebrew/<br />
Cheesemaking</a>
		  <a href="blog/exit.php?url_id=1824&amp;entry_id=95">Contact</a>
		  </td></tr>
	   	</table>
	  <p class="counter">
		
7734	  </p>
	  <p class="gensmall">unique visitors since 10/05/2004</p>	 
	  </center>


 	  </td>

	<td class="content">
	<h2>Welcome to Fabbrication.net</h2>

	<h3>News</h3>
	<p class="margin2">
	For the latest news, see the <a href="s9y/blog.php">Web Log</a>.
	</p>
<br clear="all">
	<p class="newsdate">[February 20, 2006]</p>
	<p class="margin2">
	<a href="photos.php">Pictures</a> from Europe and USA are up!
	</p>

	<p class="newsdate">[August 27, 2005]</p>
	<h4>Jocelyn and Aaron are married!</h4>
	<p class="margin2">
	It's true; Jocelyn and Aaron tied the knot.  We couldn't be
	happier.  Check out our <a href="wedding/index.php">site</a>.
	</p>

	<p class="newsdate">[Oct 30, 2004]</p>
	<h3>Aaron Fabbri's Professional Site</h3>
	<p class="margin2">
	My career/technology oriented site is
	<a href="pro/index.php">here (including Aaron Fabbri's resume).</a>.
	</p>
	<h3>Fabbri Systems</h3>
	<p class="margin2"><a href="systems/index.html">Fabbri Systems</a> is my consulting business.
	<p class="margin1">

	</p>
	<p class="margin1">
<!-- SiteSearch Google -->
<FORM method=GET action="http://www.google.com/search">
<input type=hidden name=ie value=UTF-8>
<input type=hidden name=oe value=UTF-8>
<TABLE><tr><td>
<A HREF="http://www.google.com/">
<IMG SRC="http://www.google.com/logos/Logo_40wht.gif" 
border="0" ALT="Google"></A>
</td>
<td>
<INPUT TYPE=text name=q size=31 maxlength=255 value="">
<INPUT type=submit name=btnG VALUE="Google Search">
<font size=-1>
<input type=hidden name=domains value="fabbrication.net"><br><input type=radio
name=sitesearch value=""> WWW <input type=radio name=sitesearch
value="fabbrication.net" checked>fabbrication.net<br>
</font>
</td></tr></TABLE>
</FORM>
<!-- SiteSearch Google -->
	&nbsp;
	</p>
	<p class="margin1">
	&nbsp;
	</p>

	</td> <!-- end content -->

   	</td></tr>
	<tr><td colspan="2">
	<center>
	<img src="img/mini-panorama-vancouver.jpg">
	</center>
   	</td></tr>
	</table>

	<p class="margin5">
	  <font color="#aaaaaa"> 
	  <b>Questions?</b> <a href="blog/exit.php?url_id=1824&amp;entry_id=95">Contact</a> fab.
	  </font>
	</p>


<p> &nbsp; </p>
<p> &nbsp; </p>
<!-- google analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4159952-2");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<!-- END google analytics -->
  </body>
</html>

